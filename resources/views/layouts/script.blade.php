<script type="text/javascript">
    $(function() {
        $('.lazy').lazy();
    });
</script>

<script rel="preload" as="script" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script rel="preload" as="script" src="{{ asset('js/jquery.stellar.min.js') }}"></script>
{{-- <script src="{{ asset('js/validator.js') }}"></script> --}}
<script rel="preload" as="script" src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script rel="preload" as="script" src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
<script rel="preload" as="script" src="{{ asset('js/jquery.countdown.js') }}"></script>
<script rel="preload" as="script" src="{{ asset('js/jquery.malihu.PageScroll2id.min.js') }}"></script>
<script rel="preload" as="script" src="{{ asset('js/main.js') }}"></script>

<!-- Demo Color changer Script -->
<script rel="preload" as="script" src="{{ asset('js/lmpixels-demo-panel.js') }}"></script>
<!-- /Demo Color changer Script -->
<script rel="preload" as="script" type="text/javascript" src="{{ asset('js/jquery.lazy-master/jquery.lazy.min.js') }}"></script>
<script rel="preload" as="script" type="text/javascript" src="{{ asset('js/jquery.lazy-master/jquery.lazy.plugins.min.js') }}"></script>
