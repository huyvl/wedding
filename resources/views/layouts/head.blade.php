<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>My Love</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="Our Love - Responsive Wedding Template" />
    <meta name="keywords" content="retina, responsive, wedding, rsvp, Our Love" />
    <meta name="author" content="lmpixels (Linar Miftakhov)" />
    <script type="text/javascript" nonce="c96a5d7fcb8c4952b0cf71864f8"
        src="//local.adguard.org?ts=1606637373615&amp;type=content-script&amp;dmn=lmpixels.com&amp;app=chrome.exe&amp;css=1&amp;js=1&amp;gcss=1&amp;rel=1&amp;rji=1&amp;sbe=0">
    </script>
    <script type="text/javascript" nonce="c96a5d7fcb8c4952b0cf71864f8"
        src="//local.adguard.org?ts=1606637373615&amp;name=AdGuard%20Popup%20Blocker&amp;name=AdGuard%20Assistant&amp;name=AdGuard%20Extra&amp;type=user-script">
    </script>
    <link rel="shortcut icon" href="{{ asset('images/heart.webp') }}">

    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}" type="text/css">

    <link rel="stylesheet" href="{{ asset('css/lmpixels-demo-panel.css') }}" type="text/css">

    <link rel="stylesheet" href="{{ asset('pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}">

    <!-- Optional - Adds useful class to manipulate icon font display -->
    <link rel="stylesheet" href="{{ asset('pe-icon-7-stroke/css/helper.css') }}">

    <script rel="preload" as="script" src="{{ asset('js/jquery-1.12.4.min.js') }}"></script>
    <script rel="preload" as="script" src="{{ asset('js/modernizr.custom.js') }}"></script>
</head>
