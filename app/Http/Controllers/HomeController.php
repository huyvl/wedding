<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($char, $name)
    {
        switch ($name) {
            case 1:
                $text = 'Các anh, các chị <BR>và các bạn cùng gia đình';
                break;
            case 2:
                $text = 'Bạn cùng gia đình';
                break;
            case 3:
                $text = 'Anh cùng gia đình';
                break;
            case 4:
                $text = 'Chị cùng gia đình';
                break;
            default:
                $text = 'Mỹ Linh';
        }

        $data = array(
            'char' => $char,
            'name' => $text
        );

        return view('homeUp', compact('data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $name)
    {
        switch ($name) {
            case 1:
                $text = 'Bạn Việt Anh và <BR> Em Trung Anh';
                break;
            case 2:
                $text = 'Bạn Kiên và <BR> Em Cường';
                break;
            case 3:
                $text = 'Bạn Thịnh và Thảo Vy';
                break;
            case 4:
                $text = 'Bạn Ngọc';
                break;
            case 5:
                $text = 'Anh Hùng';
                break;
            case 6:
                $text = 'Bạn Hương';
                break;
            case 7:
                $text = 'Bạn Vy & Phú';
                break;
            case 8:
                $text = 'Em Toàn Chu';
                break;
            case 9:
                $text = 'Em Hoàn';
                break;
            case 10:
                $text = 'Bạn Bình Gold Dota';
                break;
            case 11:
                $text = 'Bạn Nam';
                break;
            case 12:
                $text = 'Bạn Khẩn';
                break;
            case 13:
                $text = 'Bạn Tuán';
                break;
            case 14:
                $text = 'Em Quỳnh Anh';
                break;
            case 15:
                $text = 'Em Bình';
                break;
            case 16:
                $text = 'Anh Tuấn Anh';
                break;
            case 17:
                $text = 'Em Duyên';
                break;
            case 18:
                $text = 'Em Ngọc';
                break;
            case 19:
                $text = 'Em Tuyến';
                break;
            case 21:
                $text = 'Em An';
                break;
            case 22:
                $text = 'Em Kỳ Anh';
                break;
            case 23:
                $text = 'Em Linh Đào';
                break;
            case 24:
                $text = 'Anh Bằng';
                break;
            case 25:
                $text = 'Bạn Hùng';
                break;
            case 26:
                $text = 'Bạn Ngân';
                break;
            case 27:
                $text = 'Gia đình bạn Hà Linh';
                break;
            case 28:
                $text = 'Bạn Dung';
                break;
            case 29:
                $text = 'Bạn Thuý';
                break;
            case 30:
                $text = 'Em Phượng';
                break;
            case 31:
                $text = 'Các anh, các chị <BR>và các bạn cùng gia đình';
                break;
            case 32:
                $text = 'Bạn cùng gia đình';
                break;
            case 33:
                $text = 'Anh cùng gia đình';
                break;
            case 34:
                $text = 'Chị cùng gia đình';
                break;
            case 35:
                $text = 'Em Lý';
                break;
            case 36:
                $text = 'Em Khánh';
                break;
            default:
                $text = 'Mỹ Linh';
        }

        $data = array(
            'id' => $id,
            'name' => $text
        );

        return view('home', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
